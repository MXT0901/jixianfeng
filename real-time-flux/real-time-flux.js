/** 实时流量信息展示
 * Created by 刘汪洋 on 2017-03-25.
 */

$(document).ready(function () {
    Highcharts.setOptions({
        global: {
            useUTC: false,
        }
    });

    var min_num =0;
    var max_num = 50;

    Highcharts.chart('main', {
        chart: {
            type: 'spline',
            backgroundColor: '#fff',
           // plotBackgroundImage:"../../../../assets/img/bg.jpg",
            animation: false, //Highcharts.svg, // don't animate in old IE
            events: {
                load: function () {

                    //为图标设置每秒更新
                    var series = this.series[0];
                    setInterval(function () {
                        var x = (new Date()).getTime();// 当前时间
                        var y = Math.round(Math.random()*(max_num-min_num+1)+min_num);//模拟数据
                        series.addPoint([x, y], true, true);
                    }, 1000);
                }
            }
        },
        title: {
            text: '下单人数实时流量图',
            y:20,
            style:{
                color:'#53A93F',
                fontSize:20,
            }
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 250,
            labels:{
                style:{
                    color:'#000',
                }
            }
        },
        yAxis: {
            title: {
                text: '当前下单人数',
                style:{
                    color:'#000'
                }
            },
            allowDecimals:false,
            minTickInterval:1,
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }],
            labels:{
                style:{
                    color:'#000'
                }
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>当前下单人数</b><br/>' +
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                    Highcharts.numberFormat(this.y, 0)+"人次";
            }
        },
        legend: {
            enabled: false,
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },


        series: [{
            showInLegend: false,
            color:'#53A93F',
            data: (function () {
                // generate an array of random data
                var data = [],
                    time = (new Date()).getTime(),
                    i;
                for (i = -19; i <= 0; i += 1) {
                    data.push({
                        x: time + i * 1000,
                        y: Math.random()*50  //这里防止数据初始化瞬间下滑
                    });
                }
                //console.log(data);
                return data;
            }())
        }]
    });
});