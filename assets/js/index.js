/**
 * Created by wuli等等 on 2017/4/29.
 */
var money = echarts.init(document.getElementById('money'));

// 指定图表的配置项和数据
var option1 = {
    title:{
        text:'近一年月效益',
        left:'center',
        top:10
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            crossStyle: {
                color: '#999'
            }
        }
    },
    legend: {
        data:['服务费','抽成','总和'],
        right:10,
        top:10
    },
    xAxis: [
        {
            type: 'category',
            data: ['5月','6月','7月','8月','9月','10月','11月','12月','1月','2月','3月','4月'],
            axisPointer: {
                type: 'shadow'
            }
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: '金额/万元',
            min: 0,
            max: 50,
            interval: 10,
            axisLabel: {
                formatter: '{value}'
            }
        }
    ],
    series: [
        {
            name:'服务费',
            type:'bar',
            itemStyle:{
                normal:{
                    color:'#FFCE55',
                }
            },
            data:[3.0, 5.9, 10.0, 8.2, 10.6, 16.7, 15.6, 18.2, 22.6, 20.0, 21.4, 23.3]
        },
        {
            name:'抽成',
            type:'bar',
            itemStyle:{
                normal:{
                    color:'#53A93F',
                }
            },
            data:[2.6, 5.9, 9.0, 6.4, 8.7, 10.7, 7.6, 12.2, 13.7, 12.8, 10.6, 12.3]
        },
        {
            name:'总和',
            type:'line',
            itemStyle:{
                normal:{
                    color:'#FB6E52',
                }
            },
            data:[5.6, 11.8, 19, 14.6, 19.3, 27.4, 23.2, 30.4, 36.3, 32.8, 32.0, 35.6]
        }
    ]
};


// 使用刚指定的配置项和数据显示图表。
money.setOption(option1);

var top10 = echarts.init(document.getElementById('top'));

var option2 = {
    title: {
        text: '配送点月收益总和TOP10',
        left:'center',
        top:5
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01],
        position: 'top',
        splitLine: {show: false},
        axisLine: {show: false},
        axisTick: {show: false},
    },
    yAxis: {
        type: 'category',
        splitLine: {show: false},
        axisLine: {show: false},
        axisTick: {show: false},
        data: ['翠苑三区','滨江紫金府','西溪小筑','江湾绿苑','明江苑','永泰丰广场','东方君悦','景芳三区','悦麒美寓','五柳巷小区']
    },
    series: [
        {
            name: '收益',
            type: 'bar',
            label:{
                emphasis:{
                    show:true,
                    formatter:'{b}: {c}'+'万元'

                }
            },
            itemStyle:{
                normal:{
                    color:'#34C5E9',
                }
            },
            data: [1.6, 1.8, 1.9, 2.1, 2.2, 2.3,2.5,2.7,2.8,3.1]
        }
    ]
};

top10.setOption(option2);